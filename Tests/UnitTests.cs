﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using ResharperTestRunnerIssue;

namespace Tests
{
    public class UnitTests
    {
        [Test]
        public void InfiniteLoopHangingTheTestRunner()
        {
            // Expecting stack overflow or out of memory or something
            
            // Act

            var result = InfiniteLoopIssue.LookForInnerException(
                new Exception(
                    "inception",
                    new InvalidOperationException("10 times longer", new NotImplementedException("spin the totem"))));

            // Assert
            Assert.That(result, Is.StringContaining("totem"));
        }
    }
}
