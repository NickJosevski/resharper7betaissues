﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResharperTestRunnerIssue
{
    public static class InfiniteLoopIssue
    {
        public static string LookForInnerException(Exception ex)
        {
            var stackTrace = "";

            var digThroughException = ex;

            while (digThroughException != null && digThroughException.InnerException != null)
            {
                stackTrace += string.Format(
                    "{0}InnerException: '{1}'{0}{2}{0}",
                    Environment.NewLine,
                    ex.InnerException.Message,
                    ex.InnerException.StackTrace);

                //forgot to add this so it causes R# test runner to just keep going
                //digThroughException = digThroughException.InnerException;
            }

            return stackTrace;
        }
    }
}
